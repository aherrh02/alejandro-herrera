<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="CSS/all.js" type="text/javascript"></script>
        <link href="CSS/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="cliente?ruta=cliente">Clientes</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="cliente?ruta=producto">Productos</a>
                </li>
            </ul>
        </nav>

        <div class="p-5">

            <table class="table-striped table-bordered table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Stock</th>
                        <th>Categoria</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${productos}" var="l">
                        <tr>
                            <td><c:out value="${l.idProducto}"/></td>
                            <td><c:out value="${l.nombre}"/></td>
                            <td><c:out value="${l.precio}"/></td>
                            <td><c:out value="${l.stock}"/></td>                            
                            <td><c:out value="${l.idCategoria.nombre}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
