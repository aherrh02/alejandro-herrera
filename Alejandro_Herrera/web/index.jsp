
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <ul class="navbar-nav">
            </ul>
        </nav>
        <div class="jumbotron" style="height: 350px">
            <div class="container">
                <br/>
                <h1 class="text-center" style="font-style: italic">Bienvenido</h1>
                <div class="row mx-5">
                    <div class="col-md-5">
                        <a href="cliente?ruta=cliente" class="mt-5 btn btn-block btn-outline-info">Clientes</a>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <a href="cliente?ruta=producto" class="btn btn-block mt-5 btn-outline-info">Productos</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
