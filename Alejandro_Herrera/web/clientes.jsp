<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="CSS/all.js" type="text/javascript"></script>
        <link href="CSS/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="cliente?ruta=cliente">Clientes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cliente?ruta=producto">Productos</a>
                </li>
            </ul>
        </nav>

        <div class="p-5">

            <c:if test="${num == 0}">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong> Este cliente no tiene facturas activas </strong>
                </div>
            </c:if>

            <table class="table-striped table-bordered table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Telefono</th>
                        <th>Fecha de nacimiento</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${lista}" var="l">
                        <tr>
                            <td><c:out value="${l.idCliente}"/></td>
                            <td><c:out value="${l.nombre}"/> <c:out value="${l.apellido}"/></td>
                            <td><c:out value="${l.email}"/></td>
                            <td><c:out value="${l.telefono}"/></td>
                            <td><c:out value="${l.fechaNacimiento}"/></td>
                            <td><a href="cliente?ruta=factura&id=<c:out value="${l.idCliente}"/>" class="btn btn- btn-outline-success"><i class="fas fa-cash-register" style="color: black"></i> Ver Factura</a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
