package Controller;

import DAO.ClienteDAO;
import DAO.ProductoDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ClienteController", urlPatterns = {"/cliente"})
public class ClienteController extends HttpServlet {

    ClienteDAO clienteDao;
    ProductoDAO productoDao;

    public ClienteController() {
        clienteDao = new ClienteDAO();
        productoDao = new ProductoDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ruta = request.getParameter("ruta");
        if (ruta.equalsIgnoreCase("cliente")) {

            try {
                request.setAttribute("lista", clienteDao.allClientes());
                request.getRequestDispatcher("clientes.jsp").forward(request, response);
            } catch (Exception e) {
            }

        } else if (ruta.equalsIgnoreCase("factura")) {
            try {
                String id = request.getParameter("id");
                int factura = clienteDao.idFactura(Integer.parseInt(id));
                if (factura != 0) {
                    request.setAttribute("lista", clienteDao.facturaByCliente(factura));
                    request.getRequestDispatcher("factura.jsp").forward(request, response);
                } else {
                    request.setAttribute("lista", clienteDao.allClientes());
                    request.setAttribute("num", 0);
                    request.getServletContext().getRequestDispatcher("/clientes.jsp").forward(request, response);
                }
            } catch (Exception e) {
            }
        } else if(ruta.equalsIgnoreCase("producto")){
            try {
                request.setAttribute("lista", productoDao.allProductos());
                request.getRequestDispatcher("productos.jsp").forward(request, response);
            } catch (Exception e) {
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
