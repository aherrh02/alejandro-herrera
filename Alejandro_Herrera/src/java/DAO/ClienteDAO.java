package DAO;

import Entity.Cliente;
import Entity.Detalle;
import Entity.Factura;
import Entity.Producto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

public class ClienteDAO extends Conexion {

    Conexion cnc = new Conexion();

    public List<Cliente> allClientes() {
        List<Cliente> list = new LinkedList<>();
        try {
            cnc.Conectar();
            String sql = "Select * from Cliente";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Cliente c = new Cliente();
                c.setIdCliente(rs.getInt("id_cliente"));
                c.setNombre(rs.getString("nombre"));
                c.setApellido(rs.getString("apellido"));
                c.setDireccion(rs.getString("direccion"));
                c.setFechaNacimiento(rs.getDate("fecha_nacimiento"));
                c.setTelefono(rs.getString("telefono"));
                c.setEmail(rs.getString("email"));
                list.add(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int idFactura(int id) {
        try {
            cnc.Conectar();
            String sql = "Select * from factura where id_cliente = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int factura = rs.getInt("num_factura");
                System.out.println(factura);
                if (factura == 0) {
                    return 0;
                } else {
                    return factura;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Detalle> facturaByCliente(int id) {
        List<Detalle> list = new LinkedList<>();
        try {
            PreparedStatement pst = conn.prepareStatement("Select d.id_factura, p.nombre, d.cantidad, d.precio from detalle d inner join factura f on d.id_factura = f.num_factura inner join producto p on d.id_producto = p.id_producto where id_factura = ?");
            pst.setInt(1, id);
            ResultSet rse = pst.executeQuery();
            while (rse.next()) {
                Factura f = new Factura();
                Producto p = new Producto();
                Detalle d = new Detalle();

                f.setNumFactura(rse.getInt("id_factura"));
                d.setId_Factura(f);
                p.setNombre(rse.getString("nombre"));
                d.setId_Producto(p);
                d.setCantidad(rse.getInt("cantidad"));
                d.setPrecio(rse.getDouble("precio"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

}
