
package DAO;

import static DAO.Conexion.conn;
import Entity.Categoria;
import Entity.Cliente;
import Entity.Producto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

public class ProductoDAO extends Conexion {
    
    Conexion cnc = new Conexion();

    public List<Producto> allProductos() {
        List<Producto> list = new LinkedList<>();
        try {
            cnc.Conectar();
            String sql = "Select p.id_producto, p.nombre, p.precio, p.stock, c.nombre from producto p inner join categoria c on p.id_categoria = c.id_categoria";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Producto p = new Producto();
                Categoria c = new Categoria();
                p.setIdProducto(rs.getInt("id_producto"));
                p.setNombre(rs.getString("p.nombre"));
                p.setPrecio(rs.getDouble("precio"));
                p.setStock(rs.getInt("stock"));
                c.setNombre(rs.getString("c.nombre"));
                p.setIdCategoria(c);
                list.add(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
}
