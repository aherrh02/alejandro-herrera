package DAO;

import java.sql.*;

public class Conexion {

    private String bd = "participante13";
    private String user = "root";
    private String pass = "root";
    private String ruta = "jdbc:mysql://localhost:3306/" + bd + "?useSSL=false&allowPublicKeyRetrieval=true";

    public static Connection conn;

    public Connection getConn() {
        return conn;
    }
        
    public void setCnc(Connection cnc) {
        this.conn = conn;
    }

    public void Conectar() throws Exception {
        try {
            if (conn == null) {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(ruta, user, pass);
                if(conn != null){
                    System.out.println("Conectado");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
